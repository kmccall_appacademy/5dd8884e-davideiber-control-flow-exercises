# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    str.delete!(char) if char == char.downcase
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length%2==0
    str[str.length/2-1]+str[str.length/2]
  else
    str[str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  vowels=['a','e','i','o','u']
  str.each_char do |char|
    if vowels.include?(char.downcase)
      count+=1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product=1
  (1..num).each do |num|
    product *= num
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator="")
  string=""
  arr.each_index do |idx|
    string+=arr[idx].to_s
    string+=separator.to_s unless idx == arr.length - 1
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird=""
  chars=str.chars
  chars.each_index do |idx|
    if idx%2==0
      weird += chars[idx].downcase
    else
      weird += chars[idx].upcase
    end
  end
  weird
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  array=[]
  str.split(" ").each do |word|
    if word.length > 4
      array << word.reverse
    else
      array << word
    end
  end
  array.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result_array=[]
  (1..n).each do |num|
    if num%3 == 0 && num%5 == 0
      result_array << "fizzbuzz"
    elsif num%3 == 0
      result_array << "fizz"
    elsif num%5 == 0
      result_array << "buzz"
    else
      result_array << num
    end
  end
  result_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed_array = []
  num = arr.length - 1
  while num>-1
    reversed_array << arr[num]
    num -= 1
  end
  reversed_array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  count = 0
  (2...num).to_a.each do |i|
    if num % i == 0
      count += 1
    end
  end
  if count > 0
    false
  else
    true
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result_array=[]
  (1..num).each do |x|
    result_array << x if num%x==0
  end
  result_array
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result_array=[]
  factors(num).each do |factor|
    if prime?(factor)
      result_array << factor
    end
  end
  result_array
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_count = 0
  even_count = 0
  arr.each do |el|
    if el % 2 == 0
      even_count += 1
    else
      odd_count += 1
    end
  end
  if odd_count > even_count
    arr.each do |el|
      if el % 2 == 0
        return el
      end
    end
  else
    arr.each do |el|
      if el % 2 != 0
        return el
      end
    end
  end
end
